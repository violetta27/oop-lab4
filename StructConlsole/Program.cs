﻿using System;

namespace StructConlsole
{

    struct Airplane
    {

        public String StartCity;
        public String FinishCity;

        public Date StartDate;
        public Date FinishDate;

        public Airplane(String StartCity, String FinishCity, Date StartDate, Date FinishDate)
        {
            this.StartCity = StartCity;
            this.FinishCity = FinishCity;
            this.StartDate = StartDate;
            this.FinishDate = FinishDate;
        }


        public struct Date
        {
            public int Year;
            public int Month;
            public int Day;
            public int Hours;
            public int Minutes;

            public Date(int Year, int Month, int Day, int Hours, int Minutes)
            {
                this.Year = Year;
                this.Month = Month;
                this.Day = Day;
                this.Hours = Hours;
                this.Minutes = Minutes;
            }

        }
        public int GetTotalTime()
        {
            int tempYear = 0;
            int tempMonth = 0;
            int tempDay = 0;
            int tempHours = 0;
            int tempMinutes = 0;

            int tempDate = 0;
            int numberMonth = 0;

            if (StartDate.Month % 2 == 0) numberMonth = 30;
            else numberMonth = 31;

            tempYear = FinishDate.Year - StartDate.Year;

            tempDate = FinishDate.Month;
            if (StartDate.Month > tempDate) tempDate += 12;
            tempMonth = tempDate - StartDate.Month;

            tempDate = FinishDate.Day;
            if (StartDate.Day > tempDate) tempDate += numberMonth;
            tempDay = tempDate - StartDate.Day;

            tempDate = FinishDate.Hours;
            if (StartDate.Hours > tempDate) tempDate += 24;
            tempHours = tempDate - StartDate.Hours;

            tempDate = FinishDate.Minutes;
            if (StartDate.Minutes > tempDate) tempDate += 60;
            tempMinutes = tempDate - StartDate.Minutes;

            if (tempMinutes > 0 && tempHours != 0) tempHours--;
            if (tempHours > 0 && tempDay != 0) tempDay--;
            if (tempDay > 0 && tempMonth != 0) tempMonth--;

            /*
            String text = "Він був у дорозі:\n" +
                "Year: " + tempYear +   "\n" +
                "Month: " + tempMonth + "\n" +
                "Day: " + tempDay + "\n" +
                "Hours: " + tempHours + "\n" +
                "Minutes: " + tempMinutes + "\n";
            */

            return tempMinutes + (tempHours * 60) + (tempDay * 24 * 60) + (tempMonth * numberMonth * 24 * 60) + (tempYear * 525600);
        }

        public string DateToString()
        {
            String Date = "";

            Date = StartDate.Year.ToString();

            if (StartDate.Month < 10) Date += "0" + StartDate.Month.ToString();
            else Date += StartDate.Month.ToString();

            if (StartDate.Day < 10) Date += "0" + StartDate.Day.ToString();
            else Date += StartDate.Day.ToString();

            return Date;
        }

        public bool IsArrivingToday()
        {
            if (StartDate.Year != FinishDate.Year) return false;
            if (StartDate.Month != FinishDate.Month) return false;
            if (StartDate.Day != FinishDate.Day) return false;

            return true;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            
            Console.Title = "Лабораторна робота №4";
            Console.SetWindowSize(100, 25);

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.Clear();

            Console.WriteLine("Лабораторна робота №4");
            Console.WriteLine("Виконала Попеску В.С СН-21");
            Console.WriteLine("Варіант №18");

            Console.ReadKey();

            Console.ResetColor();

            Console.Clear();

            Airplane[] ticket;
            Console.WriteLine("Введіть кількість елементів: ");
            int n = Convert.ToInt32(Console.ReadLine());
            ticket = new Airplane[n];

            int menu = 0;

            Console.ForegroundColor = ConsoleColor.Cyan;

            while (true)
            {
                if (menu != 0) Console.ReadKey();
                Console.Clear();


                Console.WriteLine("\tМеню\n");
                Console.WriteLine("1) Записати дані в структуру;");
                Console.WriteLine("2) Знайти структуру за номером;");
                Console.WriteLine("3) Вивести всі структури;");
                Console.WriteLine("4) Максимальна та мінімальна кількість хвилин у дорозі;");
                Console.WriteLine("5) Сортування масиву структур за спаданням дати відправлення; ");
                Console.WriteLine("6) Сортування масиву структур за зростанням часу подорожі; ");
                Console.WriteLine("7) Вихід з програми");
                Console.WriteLine();
                Console.Write("Номер > ");
                menu = Convert.ToInt32(Console.ReadLine());
                
                Console.Clear();

                if (menu == 1) ReadAirplaneArray(ticket, n);
                if (menu == 2) PrintAirplane(ticket, n);
                if (menu == 3) PrintAirplanes(ticket, n);

                if (menu == 4)
                {
                    int max;
                    int min;
                    GetWorkersInfo(ticket, n, out max, out min);

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Максимальна ");
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.Write("кількість хвилин в дорозі: ");
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(max);

                    Console.WriteLine("");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("Мінімальна ");
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.Write("кількість хвилин в дорозі: ");
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(min);

                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("\n\nOK");
                }

                if (menu == 5)
                {
                    Array.Sort(ticket, SortDescendingDepartureDate);
                    Console.WriteLine("\n\nМасив відсортовано.");
                }
                if (menu == 6)
                {
                    Array.Sort(ticket, SortAscendingDepartureDate);
                    Console.WriteLine("\n\nМасив відсортовано.");
                }
                if (menu == 7) break;

                if (menu < 1 || menu > 7)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("ERROR: Введіть значення від 1 до 7");
                    menu = 0;
                    Console.ReadKey();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                }
            }       
        }

        private static void ReadAirplaneArray(Airplane[] ticket, int n)
        {
            for(int i = 0; i<n; i++)
            {
                Console.WriteLine("\t# " + Convert.ToInt32(i + 1) + " Airplane #\n");

                Console.WriteLine("Місто відправлення: ");
                ticket[i].StartCity = Console.ReadLine();
                Console.WriteLine("Місто прибуття: ");
                ticket[i].FinishCity = Console.ReadLine();
                
                Console.WriteLine("\nДата відправлення\n");

                Console.WriteLine("Рік: ");
                ticket[i].StartDate.Year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Місяць: ");
                ticket[i].StartDate.Month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("День: ");
                ticket[i].StartDate.Day = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Година: ");
                ticket[i].StartDate.Hours = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Хвилина: ");
                ticket[i].StartDate.Minutes = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("\nДата прибуття\n");

                Console.WriteLine("Рік: ");
                ticket[i].FinishDate.Year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Місяць: ");
                ticket[i].FinishDate.Month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("День: ");
                ticket[i].FinishDate.Day = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Година: ");
                ticket[i].FinishDate.Hours = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Хвилина: ");
                ticket[i].FinishDate.Minutes = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("\n---------------------------\n\n");

                Console.WriteLine("Next ->");
                Console.ReadKey();
                Console.Clear();

            }

            Console.WriteLine("\n\n#Масив структур заповнений#");
        }

        private static void PrintAirplane(Airplane[] ticket, int n)
        {
            Console.WriteLine();
            int number = 0;
            while (true)
            {
                Console.WriteLine("Введіть номер структури: ");
                number = Convert.ToInt32(Console.ReadLine());
                if (number > 0 && number < n + 1) break;
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Введіть значення від 1 до " + n + "\n\n");
                    Console.ForegroundColor = ConsoleColor.Cyan;
                }
            }
            number--;

            String tempDateDay = "";
            String tempDateMonth = "";

            String tempDateHours = "";
            String tempDateMinutes = "";

            Console.WriteLine("\t# " + Convert.ToInt32(number + 1) + " Airplane #\n");

            Console.WriteLine("Місто відправлення: " + ticket[number].StartCity);
            Console.WriteLine("Місто прибуття: " + ticket[number].FinishCity);

            if (ticket[number].StartDate.Day < 10) tempDateDay = "0" + ticket[number].StartDate.Day.ToString();
            else tempDateDay = ticket[number].StartDate.Day.ToString();
            if (ticket[number].StartDate.Month < 10) tempDateMonth = "0" + ticket[number].StartDate.Month.ToString();
            else tempDateMonth = ticket[number].StartDate.Month.ToString();

            Console.WriteLine("\nДата відправлення: " + tempDateDay + "." + tempDateMonth + "." + ticket[number].StartDate.Year);

            if (ticket[number].StartDate.Hours < 10) tempDateHours = "0" + ticket[number].StartDate.Hours.ToString();
            else tempDateHours = ticket[number].StartDate.Hours.ToString();
            if (ticket[number].StartDate.Minutes < 10) tempDateMinutes = "0" + ticket[number].StartDate.Minutes.ToString();
            else tempDateMinutes = ticket[number].StartDate.Minutes.ToString();

            Console.WriteLine("Час відправлення: " + tempDateHours + ":" + tempDateMinutes);



            if (ticket[number].FinishDate.Day < 10) tempDateDay = "0" + ticket[number].FinishDate.Day.ToString();
            else tempDateDay = ticket[number].FinishDate.Day.ToString();
            if (ticket[number].FinishDate.Month < 10) tempDateMonth = "0" + ticket[number].FinishDate.Month.ToString();
            else tempDateMonth = ticket[number].FinishDate.Month.ToString();

            Console.WriteLine("\n\nДата прибуття: " + tempDateDay + "." + tempDateMonth + "." + ticket[number].FinishDate.Year);

            if (ticket[number].FinishDate.Hours < 10) tempDateHours = "0" + ticket[number].FinishDate.Hours.ToString();
            else tempDateHours = ticket[number].FinishDate.Hours.ToString();
            if (ticket[number].FinishDate.Minutes < 10) tempDateMinutes = "0" + ticket[number].FinishDate.Minutes.ToString();
            else tempDateMinutes = ticket[number].FinishDate.Minutes.ToString();

            Console.WriteLine("Час прибуття: " + tempDateHours + ":" + tempDateMinutes);

            Console.WriteLine("\n" + ticket[number].GetTotalTime());

            Console.WriteLine("\n---------------------------\n\n");

            Console.WriteLine("\n\nOK");
        }

        private static void PrintAirplanes(Airplane[] ticket, int n)
        {
            String tempDateDay = "";
            String tempDateMonth = "";

            String tempDateHours = "";
            String tempDateMinutes = "";

            for (int i = 0; i<n; i++)
            {
                Console.WriteLine("\t# " + Convert.ToInt32(i + 1) + " Airplane #\n");

                Console.WriteLine("Місто відправлення: " + ticket[i].StartCity);
                Console.WriteLine("Місто прибуття: " + ticket[i].FinishCity);

                if (ticket[i].StartDate.Day < 10) tempDateDay = "0" + ticket[i].StartDate.Day.ToString();
                else tempDateDay = ticket[i].StartDate.Day.ToString();
                if (ticket[i].StartDate.Month < 10) tempDateMonth = "0" + ticket[i].StartDate.Month.ToString();
                else tempDateMonth = ticket[i].StartDate.Month.ToString();

                Console.WriteLine("\nДата відправлення: " + tempDateDay + "." + tempDateMonth + "." + ticket[i].StartDate.Year);

                if (ticket[i].StartDate.Hours < 10) tempDateHours = "0" + ticket[i].StartDate.Hours.ToString();
                else tempDateHours = ticket[i].StartDate.Hours.ToString();
                if (ticket[i].StartDate.Minutes < 10) tempDateMinutes = "0" + ticket[i].StartDate.Minutes.ToString();
                else tempDateMinutes = ticket[i].StartDate.Minutes.ToString();

                Console.WriteLine("Час відправлення: " + tempDateHours + ":" + tempDateMinutes);



                if (ticket[i].FinishDate.Day < 10) tempDateDay = "0" + ticket[i].FinishDate.Day.ToString();
                else tempDateDay = ticket[i].FinishDate.Day.ToString();
                if (ticket[i].FinishDate.Month < 10) tempDateMonth = "0" + ticket[i].FinishDate.Month.ToString();
                else tempDateMonth = ticket[i].FinishDate.Month.ToString();

                Console.WriteLine("\n\nДата прибуття: " + tempDateDay + "." + tempDateMonth + "." + ticket[i].FinishDate.Year);

                if (ticket[i].FinishDate.Hours < 10) tempDateHours = "0" + ticket[i].FinishDate.Hours.ToString();
                else tempDateHours = ticket[i].FinishDate.Hours.ToString();
                if (ticket[i].FinishDate.Minutes < 10) tempDateMinutes = "0" + ticket[i].FinishDate.Minutes.ToString();
                else tempDateMinutes = ticket[i].FinishDate.Minutes.ToString();

                Console.WriteLine("Час прибуття: " + tempDateHours + ":" + tempDateMinutes);

                Console.WriteLine("Кількість хвилин в дорозі: " + ticket[i].GetTotalTime());

                Console.WriteLine("\n---------------------------\n\n");

                Console.WriteLine("Next ->");
                Console.ReadKey();
                Console.Clear();
            }

            Console.WriteLine("\n\nOK");
        }

        private static void GetWorkersInfo(Airplane[] ticket, int n, out int max, out int min)
        {
            max = 0;
            min = 1000000;
            for (int i = 0; i < n; i++)
            {
                if (ticket[i].GetTotalTime() > max) max = ticket[i].GetTotalTime();
                if (ticket[i].GetTotalTime() < min) min = ticket[i].GetTotalTime();
            }

        }

        public static int SortDescendingDepartureDate(Airplane a, Airplane b)
        {
            int DateA = Convert.ToInt32(a.DateToString()), DateB = Convert.ToInt32(b.DateToString());

            if (DateA > DateB)
                return 1;
            if (DateA < DateB)
                return -1;
            return 0;


        }

        public static int SortAscendingDepartureDate(Airplane a, Airplane b)
        {
            int TimeA = a.GetTotalTime(), TimeB = b.GetTotalTime();

            if (TimeA > TimeB)
                return -1;
            if (TimeA < TimeB)
                return 1;
            return 0;
        }

    }
}